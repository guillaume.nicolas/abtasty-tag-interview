import { defineConfig } from 'cypress';

const config: Cypress.ConfigOptions = {};

export default {
  ...defineConfig(config),

  e2e: {
    setupNodeEvents(/*on, config*/) {
      // implement node event listeners here
    },
  },
};
