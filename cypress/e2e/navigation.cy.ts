describe.skip('Page navigation', () => {
  it('should login', () => {
    cy.visit('http://example.com');

    cy.wait(3000);

    cy.get('div').contains('Login').click();

    cy.get('input[type="email"]').type('johnD@example.com');
    cy.get('input[type="password"]').type('password123');
    cy.get('button[type="submit"]').click();

    cy.wait(5000);

    cy.get('.username').should('have.text', 'John Doe');
  });

  it('Should open user page on click', () => {
    cy.visit('http://example.com');

    cy.get('div').contains('Login').click();

    cy.get('input[type="email"]').type('johnD@example.com');
    cy.get('input[type="password"]').type('password123');
    cy.get('button[type="submit"]').click();

    cy.contains('Mon profil').click();

    cy.get('input#username').should('have.text', 'John Doe');
  });

  it('Should open settings page on click', () => {
    cy.visit('http://example.com');

    cy.get('div').contains('Login').click();

    cy.get('input[type="email"]').type('johnD@example.com');
    cy.get('input[type="password"]').type('password123');
    cy.get('button[type="submit"]').click();

    const header = cy.get('header');

    header.should('contain.text', `Bonjour John Doe`);

    cy.get('[data-testid="open-settings"]').click();

    header.should('contain.text', `Paramètres`);

    cy.get('input#username').should('have.text', 'John Doe');
  });

  describe('page content', () => {
    it('visits the form', () => {
      cy.visit('/users/new');
    });

    it('requires first name', () => {
      cy.get('[data-testid="first-name"]').type('Johnny');
    });

    it('requires last name', () => {
      cy.get('[data-testid="last-name"]').type('Appleseed');
    });

    it('can submit a valid form', () => {
      cy.get('form').submit();
    });
  });
});
