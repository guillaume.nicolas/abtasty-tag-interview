const HITS_ENDPOINT = 'https://hits.example.com/**';
Cypress.Commands.add('interceptHits', () => {
  cy.intercept('POST', HITS_ENDPOINT, (req) => {
    if (req.url === 'https://hits.example.com/') {
      req.alias = 'collectHit';
      req.url = 'https://hits.example.com/simu';
    } else if (req.url === 'https://hits.example.com/error') {
      req.alias = 'errorHit';
      req.url = 'https://hits.example.com/fake-error';
    }
  }).as('hit');
});
