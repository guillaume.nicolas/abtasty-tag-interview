declare namespace Cypress {
  interface Chainable<> {
    interceptHits(): Chainable<null>;
  }
}
