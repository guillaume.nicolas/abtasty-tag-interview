import { Options } from '../basics/typescript';

export function appendScript(src: string, { attributes, callback }: Options = {}) {
  const head: HTMLElement = document.getElementsByTagName('head')[0];
  const script: HTMLScriptElement = document.createElement('script');
  // @ts-expect-error onload type is not exactly callback type
  if (callback) script.onload = callback;
  script.setAttribute('type', 'text/javascript');
  script.setAttribute('src', src);
  if (attributes) {
    Object.entries(attributes).forEach(([attributeName, value]) => {
      script.setAttribute(attributeName, value);
    });
  }
  head.appendChild(script);
}
