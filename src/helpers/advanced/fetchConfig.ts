import { APIConfig, CommonConfig, Config } from '../basics/typescript';

export async function fetchConfig(accountId: string, configId: number): Promise<[Config, null] | [null, Error]> {
  const url = `https://endpoint.com/${accountId}/${configId}`;
  let modificationsFound = false;

  // @ts-expect-error variable may be undefined but ignore
  const controller: AbortController = (() => {
    try {
      return new AbortController();
    } catch (e) {
      console.warn('Cannot create AbortController', e);
    }
  })();

  const t = setTimeout(() => {
    if (!modificationsFound) {
      controller?.abort();
      console.warn(`Modifications can't be fetched for ${configId}`);
    }
  }, 3000);

  const response = await fetch(url, { signal: controller?.signal });
  return response
    .json()
    .then<[Config, null] | [null, Error]>((body: APIConfig) => {
      if (!response.ok) throw new Error('Response is not ok');
      clearTimeout(t);
      const commonConfig: CommonConfig = {
        isRemote: true,
        domain: 'endpoint.com',
        accountId,
        configurationId: configId,
      };

      const config = {
        ...body,
        ...commonConfig,
      };

      modificationsFound = true;
      return [config, null];
    })
    .catch((error: Error) => {
      clearTimeout(t);
      return [null, error];
    });
}
