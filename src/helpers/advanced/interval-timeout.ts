export function doWhen(conditionFunction: () => boolean, callback: () => void, interval: number = 100) {
  if (conditionFunction() === true) {
    callback();
  } else {
    setTimeout(() => {
      doWhen(conditionFunction, callback, interval);
    }, interval);
  }
}

export function waitFor(conditionFunction: () => boolean, interval: number = 16, timeout?: number) {
  let timer: ReturnType<typeof setTimeout>;
  return new Promise((resolve, reject) => {
    // @ts-expect-error missing return type
    const check = () => (conditionFunction() ? resolve(true) : (timer = setTimeout(check, interval)));
    check();
    if (timeout) {
      setTimeout(() => {
        clearTimeout(timer);
        reject(true);
      }, timeout);
    }
  });
}
