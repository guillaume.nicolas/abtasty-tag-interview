const config = {
  name: 'main',
  extension: 'ts',
  size: 38772,
  dir: false,
  meta: {
    creation: Date.now(),
    author: 'devid',
    id: 123123312,
  },
  content: ['Lorem ipsum dolor sit amet,', 'Nam vel iaculis metus,', 'Aliquam erat volutpat'],
};

function extractInfo(config: any) {
  const {
    name,
    meta: { author: fileAuthor },
    content,
  } = config;
  const [title, ...rows] = content;
  return {
    name,
    fileAuthor,
    title,
    body: ['#Content', ...rows].join(),
  };
}

function extract() {
  const commonInfo = {
    origin: 'abtasty',
    verbose: 'debug',
  };
  const info = extractInfo(config);
  console.log('config:', { ...commonInfo, ...info });
}

export { extract };
