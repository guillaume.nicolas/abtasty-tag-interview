function toCSV(array: string[]) {
  let str = '';
  for (const i of array) {
    str = str + i + ';';
  }
  return str;
}

const useConcat = (arr: any = []) => {
  const fakeWords = arr.length ? arr : ['foo', 'bar', 'baz', 'qux', 'quux'];
  const result = toCSV(fakeWords);
  console.log('my result', result);
};

export { useConcat };
