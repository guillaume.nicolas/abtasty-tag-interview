import { ABTestParameters, Test } from './typescript';

export function getABTestProps(tests: Test[]) {
  return tests
    .filter((t) => t.type === 'ab')
    .map(({ id, parameters }) => ({ id, parameters }))
    .reduce<ABTestParameters>((acc, t) => {
      acc[t.id] = t.parameters;
      return acc;
    }, {});
}
