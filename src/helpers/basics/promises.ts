function below50(func: any) {
  return new Promise((resolve, reject) => {
    const result = func();

    if (result < 50) {
      reject('error');
    } else {
      resolve(result);
    }
  });
}

async function pushValue(value: any) {
  await fetch('/endpoint', { method: 'post', body: JSON.stringify({ value }) });

  console.log('value posted');
}

function main() {
  const random100 = () => Math.random() * 100;

  return below50(random100)
    .then((value) => {
      console.log("it's ok", value);
      return pushValue(value);
    })
    .then((value) => {
      console.log('value is ', value);
    })
    .catch((value) => {
      console.log("it's not ok", value);
    });
}

export { main };
