export type Test = {
  type: 'ab' | 'aa' | 'bb' | 'patch' | 'xp';
  id: number;
  parameters: Record<string, any>;
  creationDate: number;
};
export type ABTestParameters = Record<Test['id'], Test['parameters']>;

export type CommonConfig = { isRemote?: boolean; domain: string; accountId: string; configurationId: number };
export type APIConfig = { filename: string; fileType: 'json' | 'pdf' | 'xml'; isDir: boolean };
export type Config = CommonConfig & APIConfig;

/* eslint-disable @typescript-eslint/no-unused-vars, @typescript-eslint/ban-types */
const HEAD: string = 'head';
const type: String = 'test/javascript';
/* eslint-enable @typescript-eslint/no-unused-vars, @typescript-eslint/ban-types */

export type Options = Partial<{
  callback: (this: HTMLElement, e: Event) => any;
  attributes: Record<string, string>;
}>;

export default { HEAD, type };
